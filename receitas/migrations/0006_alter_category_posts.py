# Generated by Django 3.2.9 on 2021-12-03 21:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('receitas', '0005_alter_category_posts'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='posts',
            field=models.ManyToManyField(blank=True, to='receitas.Post'),
        ),
    ]
