from django.db import models

# Create your models here.

from django.conf import settings
from datetime import datetime


class Post(models.Model):
    name = models.CharField(max_length=255)
    date = models.DateTimeField(default=datetime.now, blank=True)
    content = models.CharField(max_length=255)
    image_url = models.URLField(max_length=255, null=True)

    def __str__(self):
        return f'{self.name} ({self.date})'

class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    date = models.DateTimeField(default=datetime.now, blank=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'

class Category(models.Model):
    name = models.CharField(max_length=255)
    desc = models.CharField(max_length=255)
    posts = models.ManyToManyField(Post, blank=True)

    def __str__(self):
        return f'"{self.name}"'