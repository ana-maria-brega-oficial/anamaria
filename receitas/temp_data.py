receita_data = [{
    "id":
    "1",
    "name":
    "Cookies de Chocolate",
    "release_year":
    "2018",
    "image_url":
    "https://cdn.panelinha.com.br/receita/1601926621338-cookie.jpg"
}, {
    "id":
    "2",
    "name":
    "Bolo de Leite Ninho",
    "release_year":
    "2019",
    "image_url":
    "https://receitatodahora.com.br/wp-content/uploads/2018/09/creme-de-leite-ninho-1.jpg"
}, {
    "id":
    "3",
    "name":
    "Panqueca",
    "release_year":
    "2019",
    "image_url":
    "https://www.receitasedicasdochef.com.br/wp-content/uploads/2019/05/Panquecas-Americanas-de-Chocolate.jpg"
}]
