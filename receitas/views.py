from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from .temp_data import receita_data

from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy

from django.shortcuts import render, get_object_or_404

from django.views import generic

from .models import Post, Comment, Category
from .forms import CommentForm



class CategoryListView(generic.ListView):
    model = Category
    template_name = 'receitas/category_list.html'

class CategoryDetailView(generic.DetailView):
    model = Category
    template_name = 'receitas/category.html'

def create_comment(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment = Comment(author=comment_author,
                            text=comment_text,
                            post=post)
            comment.save()
            return HttpResponseRedirect(
                reverse('receitas:detail', args=(post_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'post': post}
    return render(request, 'receitas/comment.html', context)

class ReceitaListView(generic.ListView):
    model = Post
    template_name = 'receitas/index.html'

class ReceitaDetailView(generic.DetailView):
    model = Post
    template_name = 'receitas/detail.html'

class ReceitaCreateView(generic.CreateView):
    model = Post
    fields = [
            'name',
            'content',
            'image_url',
        ]
    template_name = 'receitas/create.html'
    success_url = '../'

class ReceitaUpdateView(generic.UpdateView):
    model = Post
    fields = [
            'name',
            'content',
            'image_url',
        ]
    template_name = 'receitas/update.html'
    success_url = '../../'

class ReceitaDeleteView(generic.DeleteView):
    model = Post
    template_name = 'receitas/delete.html'
    success_url = '../../'



# def list_receitas(request):
#     receita_list = Post.objects.all()
#     context = {'receita_list': receita_list}
#     return render(request, 'receitas/index.html', context)

# def detail_receita(request, receita_id):
#     receita = get_object_or_404(Post, pk=receita_id)
#     context = {'receita': receita}
#     return render(request, 'receitas/detail.html', context)

# def create_receita(request):
#     if request.method == 'POST':
#         form = PostForm(request.POST)
#         if form.is_valid():
#             receita_name = request.POST['name']
#             receita_content = request.POST['content']
#             receita_image_url = request.POST['image_url']
#             receita = Post(name=receita_name,
#                       content=receita_content,
#                       image_url=receita_image_url)
#             receita.save()
#             return HttpResponseRedirect(
#                  reverse('receitas:detail', args=(receita.id, )))
#     else:
#         form = PostForm()
#     context = {'form': form}
#     return render(request, 'receitas/create.html', context)

# def update_receita(request, receita_id):
#     receita = get_object_or_404(Post, pk=receita_id)

#     if request.method == "POST":
#         form = PostForm(request.POST)
#         if form.is_valid():
#             receita.name = request.POST['name']
#             receita.content = request.POST['content']
#             receita.image_url = request.POST['image_url']
#             receita.save()
#             return HttpResponseRedirect(
#                 reverse('receitas:detail', args=(receita.id, )))
#     else:
#         form = PostForm(
#             initial={
#                 'name': receita.name,
#                 'content': receita.content,
#                 'image_url': receita.image_url
#             })

#     context = {'receita': receita, 'form': form}
#     return render(request, 'receitas/update.html', context)

# def delete_receita(request, receita_id):
#     receita = get_object_or_404(Post, pk=receita_id)

#     if request.method == "POST":
#         receita.delete()
#         return HttpResponseRedirect(reverse('receitas:index'))

#     context = {'receita': receita}
#     return render(request, 'receitas/delete.html', context)


