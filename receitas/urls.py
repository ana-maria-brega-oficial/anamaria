from django.urls import path

from . import views

app_name = 'receitas'
urlpatterns = [
    path('', views.ReceitaListView.as_view(), name='index'),
    path('create/', views.ReceitaCreateView.as_view(), name='create'),
    path('<int:pk>/', views.ReceitaDetailView.as_view(), name='detail'),
    path('update/<int:pk>/', views.ReceitaUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', views.ReceitaDeleteView.as_view(), name='delete'),
    path('<int:post_id>/comment/', views.create_comment, name='comment'),
    path('category_list/', views.CategoryListView.as_view(), name='category_list'),
    path('category/<int:pk>/', views.CategoryDetailView.as_view(), name='category'),
]
